import { HW13AppPage } from './app.po';

describe('hw13-app App', function() {
  let page: HW13AppPage;

  beforeEach(() => {
    page = new HW13AppPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
