import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-my-component',
  template: `<h2>
    {{title}}
    </h2>
    <ul>
    <li appUpper *ngFor="let item of _stringArray">{{item}}</li>
    </ul>`,
  styles: []
})
export class MyComponentComponent implements OnInit {  
  title="Welcome To My Component";
   _stringArray:string[]=[];
   @Input()
   set stringArray(sa:string[]){
     this._stringArray=sa;
   }
  constructor() {
  }
  ngOnInit() {
  }
}
