import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title:string = 'Welcome To Homework13!';
  strArr:string[]=["Software Engineering","Web Application Programming","Modern Web Applications","Algorithm"];
}
